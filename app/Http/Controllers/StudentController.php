<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Student;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\UploadedFile;
use App\Http\Resources\StudentResource;
use App\Http\Requests\StoreStudentRequest;
use Symfony\Component\Console\Input\Input;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Requests\BulkAttendanceRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $students = Student::orderBy('attendance', 'desc')->get();

        return StudentResource::collection($students);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudentRequest $request)
    {
        $validated = $request->validated();

        if ($request->image  && $request->image instanceof UploadedFile) {
            $image_extension = $request->image->getClientOriginalExtension();
            $image_name = time() . '.' . $image_extension;
            $request->image->move(public_path('images'), $image_name);
            $validated['image'] = $image_name;
        }

        Student::create($validated);

        return response()->json(['success' => 'Successfully Created!'], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        return StudentResource::make($student);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {

        $validated = $request->validated();

        if ($request->image  && $request->image instanceof UploadedFile) {
            $image_extension = $request->image->getClientOriginalExtension();
            $image_name = time() . '.' . $image_extension;
            $request->image->move(public_path('images'), $image_name);
            $validated['image'] = $image_name;
        }

        $student->update($validated);

        return response()->json(['success' => 'Successfully Updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {
        $student->delete();

        return response()->json(['success' => 'Successfully Deleted!'], 200);
    }

    public function is_attendance(Request $request)
    {
        // The request has two values id and is-attendance
        // Loop on the json code to be able to access id
        // Check if the student attendance
        // Increase the attendance if the student attendance

        $attendance_students = $request->all();
        foreach ($attendance_students as $attendance_student) {
            if ($attendance_student['is-attendance']) {
                $student = Student::find($attendance_student['id']);
                if ($student) {
                    $student->increment('attendance');
                }
            }
        }
        return response()->json(['success' => 'Students Has Been Checked Successfully'], 200);
    }
}
